#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

using namespace std;

int Figlio() {
	return 6 - 2;
}

int main(int argc, char *argv[]) {
	pid_t pid;
	int x, y, z;
	
	pid = fork();
	if (pid == 0) {
		y = Figlio();
		exit(y);
	} else {
		x = 3 + 2;
		waitpid(pid, &y, 0);
		z = x + WEXITSTATUS(y);
		cout << "x = " << x << endl
			 << "y = " << WEXITSTATUS(y) << endl
			 << "z = " << z;
	}
}