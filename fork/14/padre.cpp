#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

int main() {
	char cognome[100];
	int eta;
	pid_t pid;
	
	pid = fork();
	if (pid == 0) {
		execl("figlio", NULL);
		cout << "Errore nell'esecuzione del figlio." << "\n";
		exit(1);
	}

	waitpid(pid, &eta, 0);
	
	cout << "Inserire il nome : ";
	cin >> cognome;
	cout << cognome << " ha " << WEXITSTATUS(eta)<<" anni.";
	
	
}