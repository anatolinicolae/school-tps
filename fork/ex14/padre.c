#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
	char cognome[100];
	int eta;
	pid_t pid;
	
	pid = fork();
	if (pid == 0) {
		execl("figlio", NULL);
		printf("Errore nell'esecuzione del figlio.");
		exit(1);
	}

	waitpid(pid, &eta, 0);
	
	printf("Inserisci il tuo nome: ");
	scanf("%s", &cognome);
	printf("%s ha %d anni.", cognome, WEXITSTATUS(eta));
	
	
}