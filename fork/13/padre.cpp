#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

using namespace std;

int main(){
	pid_t pid;
	int x,y,z;
	
	pid=fork();
	if (pid==0) {
		execl("figlio",NULL);
		printf("Esecuzione del figlio fallita.");
		exit(-1);
	} else {
		x=3+2;
		waitpid(pid,&y,0);
		z=x+WEXITSTATUS(y);
		cout << "x: " << x <<"\n";
		cout << "y: " << WEXITSTATUS(y) <<"\n";
		cout << "z: " << z;
	}
	
	return 0;
}