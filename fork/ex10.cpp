#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

using namespace std;
int main(int argc, char *argv[]) {
	pid_t pid[4];
	int status[4];
	
	pid[0] = fork();
	if (pid == 0) {
		sleep(1);
		exit(EXIT_SUCCESS);
	} else {
		pid[1] = fork();
		if (pid == 0) {
			sleep(2);
			exit(EXIT_SUCCESS);
		} else {
			pid[2] = fork();
			if (pid == 0) {
				sleep(3);
				exit(EXIT_SUCCESS);
			} else {
				pid[3] = fork();
				if (pid == 0) {
					sleep(4);
					exit(EXIT_SUCCESS);
				} else {
					waitpid(pid[0], &status[0], 0);
					cout << "PID:    " << getpid() << endl
						 << "Status: " << WEXITSTATUS(status[0]);
					waitpid(pid[1], &status[1], 0);
					cout << "PID:    " << getpid() << endl
						 << "Status: " << WEXITSTATUS(status[1]);
					waitpid(pid[2], &status[2], 0);
					cout << "PID:    " << getpid() << endl
						 << "Status: " << WEXITSTATUS(status[2]);
					waitpid(pid[3], &status[3], 0);
					cout << "PID:    " << getpid() << endl
						 << "Status: " << WEXITSTATUS(status[3]);
				}
			}
		}
	}
}