#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

using namespace std;
int main(int argc, char *argv[]) {
	pid_t pid;
	int status;
	
	pid = fork();
	if (pid == 0) {
		sleep(4);
		exit(EXIT_SUCCESS);
	} else {
		waitpid(pid, &status, 0);
		cout << "PID:    " << getpid() << endl
			 << "Status: " << WEXITSTATUS(status);
	}
}