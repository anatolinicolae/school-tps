import java.io.*;

class Threaddino implements Runnable {
	private String name;
		
	public Threaddino() {
		this.name = getClass().getName();
	}
	
	public Threaddino(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		System.out.println(name);
	}

	public static void main(String[] args) {
		// Start threads
		Thread tid;
		int repeat;
		
		System.out.print("Quante volte dev'essere ripetuta la sequenza? ");
		
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			
			repeat = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			repeat = 0;
		}
		
		long start = System.currentTimeMillis();
		
		try {
			for (int i = 0; i < repeat; i++) {
				tid = new Thread(new Threaddino("din"));
				tid.start();
				tid.join();
				
				tid = new Thread(new Threaddino("don"));
				tid.start();
				tid.join();
				
				tid = new Thread(new Threaddino("dan"));
				tid.start();
				tid.join();
			}
		} catch (Exception e) { }
		
		long end = System.currentTimeMillis();
		
		System.out.println("Totale ms esecuzione: " + (end - start));
	}
}