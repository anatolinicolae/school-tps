class PingPong implements Runnable {
	private String name;
	private int sleep = 0;
	
	public PingPong() {}
	public PingPong(String name) {
		this.name = name;
	}
	public PingPong(String name, int sleep) {
		this.name = name;
		this.sleep = sleep;
	}
	
	public void run() {
		for (int i = 0; i < 50; i++) {
			System.out.println(name);
			try { Thread.sleep(sleep); } catch (Exception e) { }
			Thread.yield();
		}
	}
	
	public static void main(String[] args) {
		// Start threads
		(new Thread(new PingPong("Ping", 1000))).start();
		(new Thread(new PingPong("Pong", 1000))).start();
	}
}