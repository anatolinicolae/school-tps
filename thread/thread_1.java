class ThreadTests {
	public static void main(String[] args) {
		Settenani t = new Settenani("Eccolo");
		t.start();
	}
}

class Settenani extends Thread {
	public Settenani() {}
	public Settenani(String name) { setName(name); }
	
	@Override
	public void run() {
		System.out.println("Nome thread: " + getName());
		
		for (int i = 1; i <= 7; i++)
			System.out.println(i);
	}
}