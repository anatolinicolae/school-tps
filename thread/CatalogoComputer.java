//package catalogocomputer;

/**
 * Utile per l'input
 * http://stackoverflow.com/questions/10773132/how-to-unfocus-a-jtextfield/10773412#10773412
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
//import catalogocomputer.*;
import java.util.ArrayList;
import javax.swing.table.*;

public class CatalogoComputer implements ActionListener, Runnable {

	protected ArrayList<Computer> computer;
	private JFrame frame;
	private JPanel panel;
	private JButton add_new, show_latest, show_code, show_count;
	private JScrollPane scroll;
	private static String[] columns = {"Code", "Brand", "Model", "CPU", "RAM", "HDD", "Year"};
	private DefaultTableModel model;
	private JTable table;

	public CatalogoComputer() {
		frame = new JFrame();
		panel = new JPanel();
		model = new DefaultTableModel(columns, 0);
		table = new JTable(model);
		add_new = new JButton("Add new");
		show_latest = new JButton("Show latest");
		show_code = new JButton("Show by code");
		show_count = new JButton("Brand count");
		computer = new ArrayList<Computer>();

		computer.add(new Computer("Apple", "MacBook", 2.3, 8.0, 500.0, 2011));
		int latest = model.getRowCount();
		model.addRow(new String[]{
					computer.get(latest).getCodice(),
					computer.get(latest).getMarca(),
					computer.get(latest).getModello(),
					computer.get(latest).getVelproc() + "",
					computer.get(latest).getRam() + "",
					computer.get(latest).getHdd() + "",
					computer.get(latest).getAnno() + ""
				});

		scroll = new JScrollPane(table);
		scroll.setBounds(10, 10, 400, 144);

		add_new.setBounds(420, 10, 124, 28);
		show_latest.setBounds(420, 48, 124, 28);
		show_code.setBounds(420, 86, 124, 28);
		show_count.setBounds(420, 124, 124, 28);

		panel.setLayout(null);
		panel.add(scroll);
		panel.add(add_new);
		panel.add(show_latest);
		panel.add(show_code);
		panel.add(show_count);

		add_new.addActionListener(this);
		show_latest.addActionListener(this);
		show_code.addActionListener(this);
		show_count.addActionListener(this);

		frame.setTitle("MediaWorld");
		frame.add(panel);
		frame.setSize(560, 190);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void showModalInsert(JFrame frame, String b,
			String m, String c, String r, String h, String y) {
		JPanel p = new JPanel(new BorderLayout(5, 5));

		JPanel labels = new JPanel(new GridLayout(0, 1, 2, 2));
		labels.add(new JLabel("Brand", SwingConstants.RIGHT));
		labels.add(new JLabel("Model", SwingConstants.RIGHT));
		labels.add(new JLabel("CPU", SwingConstants.RIGHT));
		labels.add(new JLabel("RAM", SwingConstants.RIGHT));
		labels.add(new JLabel("HDD", SwingConstants.RIGHT));
		labels.add(new JLabel("Year", SwingConstants.RIGHT));
		p.add(labels, BorderLayout.WEST);

		JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
		JTextField brand = new JTextField(b);
		JTextField pcmodel = new JTextField(m);
		JTextField cpu = new JTextField(c);
		JTextField ram = new JTextField(r);
		JTextField hdd = new JTextField(h);
		JTextField year = new JTextField(y);
		controls.add(brand);
		controls.add(pcmodel);
		controls.add(cpu);
		controls.add(ram);
		controls.add(hdd);
		controls.add(year);
		p.add(controls, BorderLayout.CENTER);

		JOptionPane.showMessageDialog(frame, p,
				"Add new", JOptionPane.QUESTION_MESSAGE);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		if (!brand.getText().isEmpty()
				&& !pcmodel.getText().isEmpty()
				&& !cpu.getText().isEmpty()
				&& !ram.getText().isEmpty()
				&& !hdd.getText().isEmpty()
				&& !year.getText().isEmpty()) {
			try {
				computer.add(new Computer(
						brand.getText(),
						pcmodel.getText(),
						Double.parseDouble(cpu.getText()),
						Double.parseDouble(ram.getText()),
						Double.parseDouble(hdd.getText()),
						Integer.parseInt(year.getText())));
				int latest = model.getRowCount();
				model.addRow(new String[]{
							computer.get(latest).getCodice(),
							computer.get(latest).getMarca(),
							computer.get(latest).getModello(),
							computer.get(latest).getVelproc() + " GHz",
							computer.get(latest).getRam() + " GB",
							computer.get(latest).getHdd() + " GB",
							computer.get(latest).getAnno() + ""
						});
			} catch (Exception e) {
				int option = JOptionPane.showConfirmDialog(
						frame,
						"Input Error. Want to retry?",
						"Error",
						JOptionPane.YES_NO_OPTION);
				if (option == JOptionPane.YES_OPTION) {
					showModalInsert(
							frame,
							brand.getText(),
							pcmodel.getText(),
							cpu.getText(),
							ram.getText(),
							hdd.getText(),
							year.getText());
				}
			}
		}
	}

	private void showModalInsert(JFrame frame) {
		showModalInsert(frame, "", "", "", "", "", "");
	}

	public int getLatestID() {
		int year = 0, pos = -1;

		for (int i = 0; i < computer.size(); i++) {
			if (computer.get(i).getAnno() > year) {
				year = computer.get(i).getAnno();
				pos = i;
			}
		}

		return pos;
	}

	public void showModalLatest() {
		String pcInfo;
		int id = getLatestID();

		if (id > -1) {
			pcInfo = computer.get(id).toString();
		} else {
			pcInfo = "No computers available actually.";
		}

		JOptionPane.showMessageDialog(
				frame,
				pcInfo,
				"Latest PC",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public int get_by_code(String code) {
		int pos = 0;

		while (pos < computer.size() && !computer.get(pos).getCodice().equals(code)) {
			pos++;
		}

		if (pos == computer.size()) {
			pos = -1;
		}

		return pos;
	}

	public void showModalByCode() {
		String search_code = JOptionPane.showInputDialog(
				null,
				"Insert computer code?",
				"Search",
				JOptionPane.QUESTION_MESSAGE);
		try {
			if (search_code != null) {
				int codepos = get_by_code(search_code);
				JOptionPane.showMessageDialog(
					frame,
					computer.get(codepos).toString(),
					"Search",
					JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(
					frame,
					"No computer found with code: " + search_code,
					"Search",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public int get_by_brand(String brand) {
		int c = 0;

		for (int i = 0; i < computer.size(); i++) {
			if (computer.get(i).getMarca().equals(brand)) {
				c++;
			}
		}

		return c;
	}

	public void showModalByBrand() {
		String search_brand = JOptionPane.showInputDialog(
				null,
				"Insert computer brand?",
				"Search",
				JOptionPane.QUESTION_MESSAGE);
		
		if (search_brand != null) {
			int search_count = get_by_brand(search_brand);
			String status;
			if (search_count > 0) {
				status = search_brand + " computers: " + search_count;
			} else {
				status = "No " + search_brand + " brand found.";
			}
			JOptionPane.showMessageDialog(
				frame,
				status,
				"Search",
				JOptionPane.INFORMATION_MESSAGE);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "Add new":
				showModalInsert(frame);
				break;
			case "Show latest":
				showModalLatest();
				break;
			case "Show by code":
				showModalByCode();
				break;
			case "Brand count":
				showModalByBrand();
				break;
		}
	}
	
	public void run() {
		new CatalogoComputer();
	}

	public static void main(String[] args) {
		(new Thread(new CatalogoComputer())).start();
		(new Thread(new CatalogoComputer())).start();
	}
}