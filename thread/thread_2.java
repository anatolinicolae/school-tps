class ThreadTests {
	public static void main(String[] args) {
		new Randomizor("Attributo").start();
		new Randomizor("Metodo").start();
	}
}

class Randomizor extends Thread {
	public Randomizor() {}
	public Randomizor(String name) { setName(name); }
	
	@Override
	public void run() {
		System.out.println("Nome thread: " + getName());
		
		/*for (int i = 1; i <= 7; i++)
			System.out.print(i);*/
	}
}