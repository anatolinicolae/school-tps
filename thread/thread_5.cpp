#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>

using namespace std;

// Funzione thread
void *threadCode(void *arg) {
	cout << (char *) arg << endl;
	pthread_exit((void *)0);
}

int main(int argc, char *argv[]) {
	// Pid del thread
	pthread_t pid[2];
	char str[3][4] = { "Qui", "Quo", "Qua" };
	
	// Display stringa random
	cout << "Ciao, sono il main thread." << endl;
	
	// Creazione thread
	for (int i = 0; i < 3; i++) {
		pthread_create(&pid[i], NULL, threadCode, (void *)str[i]);
	}
	
	sleep(2);
	
	return 0;
}