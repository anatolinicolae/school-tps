class Campana implements Runnable {
	private String name;
	private int sleep = 0;
	
	public Campana() {}
	public Campana(String name) {
		this.name = name;
	}
	public Campana(String name, int sleep) {
		this.name = name;
		this.sleep = sleep;
	}
	
	public void run() {
		try { Thread.sleep(sleep); } catch (Exception e) { }
		System.out.println(name);
	}
	
	public static void main(String[] args) {
		String[] campane = {"Din", "Don", "Dan", "Din", "Don", "Dan"};
		
		// Start threads
		for (int i = 0; i < 6; i++) {
			(new Thread(new Campana(campane[i], 250*(i+1)))).start();
			(new Thread(new Campana("Dun", 260 * (i+1)))).start();
		}
	}
}