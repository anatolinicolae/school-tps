#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>

using namespace std;

// Funzione thread
void *thread1(void *arg) {
	cout << (char *) arg << endl;
	pthread_exit((void *)0);
}

void *thread2(void *arg) {
	cout << (char *) arg << endl;
	pthread_exit((void *)0);
}

int main(int argc, char *argv[]) {
	// Pid del thread
	pthread_t pid[2];
	
	// Display stringa random
	cout << "Ciao, sono il main thread." << endl;
	
	// Creazione thread
	pthread_create(&pid[0], NULL, thread1, (void *)"Ciao");
	pthread_create(&pid[1], NULL, thread2, (void *)"Utente");
	
	sleep(2);
	
	return 0;
}