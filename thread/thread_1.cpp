#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
using namespace std;

// Funzione thread
void *viewThread(void *args) {
	// Display pid thread
	cout << "PID:  " << getpid() << endl;
	return NULL;
}

int main(int argc, char *argv[]) {
	// Pid del thread
	pthread_t pid;
	
	// Display del pid del padre
	cout << "PPID: " << getpid() << endl;
	
	// Creazione thread
	pthread_create(&pid, NULL, viewThread, NULL);
	
	// Finta attesa del thread
	sleep(1);
}