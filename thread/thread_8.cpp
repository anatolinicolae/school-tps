#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
using namespace std;

int c1 = 200, c2 = 300;
bool ver = false;

void *operazione(void * arg) {
	int *numero;
	
	numero = (int *)arg;
	
	c1 = c1 - (*numero);
	c2 = c2 + (*numero);
	
	pthread_exit((void *) numero);
}

int main(int argc, char *argv[]) {
	pthread_t tid[2];
	int n1 = 10, n2 = 20;
	
	pthread_create(&tid[0], NULL, operazione, (void *)&n1);

	pthread_join(tid[0], NULL);
	cout << "Conto 1: "<< c1 << ", Conto 2: " << c2 << endl;

	pthread_create(&tid[1], NULL, operazione, (void *)&n2);

	pthread_join(tid[1], NULL);
	cout << "Conto 1: "<< c1 << ", Conto 2: " << c2 << endl;
	
	cout << "Somma: " << c1 + c2 << endl;
	
	return 0;
}