#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
using namespace std;

// Funzione thread
void *viewThread(void *args) {
	// Display countdown
	for (int i = 1; i < 11; i++) {
		cout << i << endl;
		sleep(1);
	}
	cout << "Countdown concluso!" << endl;
	return NULL;
}

int main(int argc, char *argv[]) {
	// Pid del thread
	pthread_t pid;
	
	// Display del pid del padre
	cout << "Il thread inizierà un countdown." << endl;
	
	// Creazione thread
	pthread_create(&pid, NULL, viewThread, NULL);
	
	// Finta attesa del thread
	sleep(11);
}