import java.io.*;
import java.nio.channels.*;

class Threaddino implements Runnable {
	private int repeat;
	private boolean pari;
		
	public Threaddino() {
		repeat = 0;
		pari = true;
	}
	
	public Threaddino(int repeat, boolean pari) {
		this.repeat = repeat;
		this.pari = pari;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < repeat; i++) {
			if (pari) {
				if (i % 2 == 0) {
					System.out.println("Pari\t" + i);
				}
			} else {
				if (i % 2 != 0) {
					System.out.println("Dispari\t" + i);
				}
			}
			try { Thread.sleep(100); } catch (Exception e) { }
			Thread.currentThread().yield();
		}
	}

	public static void main(String[] args) {
		// Start threads
		Thread tid1, tid2;
		int repeat;
		
		System.out.print("Quanti numeri devono essere visualizzati? ");
		
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			
			repeat = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			repeat = 0;
		}
		
		long start = System.currentTimeMillis();
		
		try {
			tid1 = new Thread(new Threaddino(repeat, true));
			tid1.start();

			tid2 = new Thread(new Threaddino(repeat, false));
			tid2.start();
			
			tid1.join();
			tid2.join();
		} catch (Exception e) { }
		
		long end = System.currentTimeMillis();
		
		System.out.println("Totale ms esecuzione: " + (end - start));
	}
}