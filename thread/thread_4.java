import java.math.*;

class Campana implements Runnable {
	private String name;
	private int repeat = 0;
	
	public Campana() {}
	public Campana(String name) {
		this.name = name;
	}
	public Campana(String name, int repeat) {
		this.name = name;
		this.repeat = repeat;
	}
	
	public void run() {
		for (int i = 0; i < repeat; i++) {
			System.out.println(name);
		}
	}
	
	public static void main(String[] args) {
		String[] campane = {"Din", "Don", "Dan"};
		
		// Start threads
		for (int i = 0; i < 3; i++) {
			(new Thread(new Campana(campane[i], (int) (Math.random() * 5) + 1))).start();
		}
	}
}