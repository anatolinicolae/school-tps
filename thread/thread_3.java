class ThreadTests {
	public static void main(String[] args) {
		String[] nani = {"Eccolo", "Idolo", "Mestolo", "Pendolo",
						 "Tavolo", "Dammelo", "Ergastolo"};
		
		// Start threads
		for (int i = 0; i < 7; i++) {
			new Settenani(nani[i]).start();
		}
	}
}

class Settenani extends Thread {
	public Settenani() {}
	public Settenani(String name) { setName(name); }
	
	@Override
	public void run() {
		System.out.println("Nome thread: " + getName());
		
		/*for (int i = 1; i <= 7; i++)
			System.out.println(i);*/
	}
}