//package catalogocomputer;

class Computer {
	protected String codice, marca, modello;
	protected Double velproc, ram, hdd;
	protected int anno;
	static protected int latest = 0;
	
	public Computer(String marca, String modello, Double velproc, Double ram, Double hdd, int anno) {
		this.codice = "C00" + this.latest;
		this.latest++;
		this.marca = marca;
		this.modello = modello;
		this.velproc = velproc;
		this.ram = ram;
		this.hdd = hdd;
		this.anno = anno;
	}

	public String getCodice() {
		return codice;
	}

	public String getMarca() {
		return marca;
	}

	public String getModello() {
		return modello;
	}

	public Double getVelproc() {
		return velproc;
	}

	public Double getRam() {
		return ram;
	}

	public Double getHdd() {
		return hdd;
	}

	public int getAnno() {
		return anno;
	}

	public String toString() {
		return marca + " " + modello + " (" + anno + "): CPU " + velproc + " GHz, RAM " + ram + " GB, HDD " + hdd + " GB";
	}
}