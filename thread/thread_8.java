import java.awt.*;

class Nephew implements Runnable {
	private 		String	name;
	private			int		sleep = 0,
							metri = 0;
	private static	boolean	hasWinner = false;
	
	public Nephew() {}
	public Nephew(String name) {
		this.name = name;
	}
	public Nephew(String name, int sleep) {
		this.name = name;
		this.sleep = sleep;
	}
	
	@Override
	public void run() {
		while (metri < 100 && !hasWinner) {
			metri += (int) (Math.random() * 15) + 1;
			if (metri < 100) {
				System.out.println(name + " " + metri + " m");
				try { Thread.sleep(metri * 10); } catch (Exception e) { }
			} else {
				System.out.println(name + " Finished!");
				hasWinner = true;
			}
			Thread.yield();
		}
	}

	public static void main(String[] args) {
		// Start threads
		(new Thread(new Nephew("Qui"))).start();
		(new Thread(new Nephew("Quo"))).start();
		(new Thread(new Nephew("Qua"))).start();
	}
}