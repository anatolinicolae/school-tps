class Campana implements Runnable {
	private String name;
	private int repeat = 0;
	
	public Campana() {}
	public Campana(String name) {
		this.name = name;
	}
	public Campana(String name, int repeat) {
		this.name = name;
		this.repeat = repeat;
	}
	
	public void run() {
		try { Thread.sleep(250 * repeat); } catch (Exception e) { }
		System.out.println(name);
	}
	
	public static void main(String[] args) {
		String[] campane = {"Din", "Don", "Dan", "Din", "Don", "Dan"};
		
		// Start threads
		for (int i = 0; i < 6; i++) {
			(new Thread(new Campana(campane[i], i + 1))).start();
		}
	}
}