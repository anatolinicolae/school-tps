#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
using namespace std;

void *Raddoppia (void * arg) {
	int *numero;
	
	numero = (int *)arg;
	*numero = (*numero) * 2;
	pthread_exit((void *) numero);
}

int main(int argc, char *argv[]) {
	pthread_t tid;
	int n = 3, *ris;
	
	pthread_create(&tid, NULL, Raddoppia, (void *)&n);
	pthread_join(tid, (void **)&ris);
	
	cout << *ris << endl;
	cout << n << endl;

	return 0;
}